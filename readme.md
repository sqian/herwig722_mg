# How to use

1. Basically only use `lhe.in`
2. Detailed commands are summarized in `.gitlab-ci.yml`, from `script` section, `before-script` section is of no use.
3. Further parts refer to https://herwig.hepforge.org/tutorials/workflow/inputoutput.html?highlight=hepmc
